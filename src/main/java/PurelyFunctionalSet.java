import java.util.Objects;
import java.util.function.Function;
import java.util.function.Predicate;

@FunctionalInterface
public interface PurelyFunctionalSet<T> {
    boolean contains(T Element);
}
